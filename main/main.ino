/*
 * Source  : https://github.com/AndreaLombardo/L298N/
 */
// Include the (new) library
#include <L298NX2.h>

// Pin definition
const unsigned int EN_A = 9;
const unsigned int IN1_A = 7;
const unsigned int IN2_A = 6;

const unsigned int IN1_B = 5;
const unsigned int IN2_B = 4;
const unsigned int EN_B = 3;

const int pinInfraredSensorLeft = A0;
const int pinInfraredSensorRight = A1;

// Initialize both motors
L298NX2 motors(EN_A, IN1_A, IN2_A, EN_B, IN1_B, IN2_B);

int speedness = 191;
int time_rotation = 10;

/**
 * Detect if the distance between our robot and the opponent is lower than 20cm
*/
int is_line_detected(int value_captor)
{
  return value_captor < 850;
}

/**
 * Rotate backward the right motor
*/
void rotate_motor_right()
{
  motors.stop();
  delay(100);
  motors.backwardB();
  delay(time_rotation);
}

/**
 * Rotate backward the left motor
*/
void rotate_motor_left()
{
  motors.stop();
  delay(100);
  motors.backwardA();
  delay(time_rotation);
}

void setup()
{
  // Used to display information
  Serial.begin(9600);
}

void loop()
{
  // Apply faded speed to both motors
  motors.setSpeedA(speedness);
  motors.setSpeedB(speedness);

  // Tell motor A and B to go forward (may depend by your wiring)
  //motors.forwardA();
  //motors.forwardB();

  // Get information from sensor
  int left_sensor_value = analogRead(pinInfraredSensorLeft);
  int right_sensor_value = analogRead(pinInfraredSensorRight);
  Serial.print("Capteur Gauche : ");
  Serial.println(left_sensor_value);
  Serial.print("Capteur Droite : ");
  Serial.println(right_sensor_value);
  
  // Rotate if the robot is close to the out of the board
  /*if (is_line_detected(left_sensor_value)) {
    rotate_motor_right();
  } else if (is_line_detected(right_sensor_value)) {
    rotate_motor_left();
  } */
}
